module.exports = {
  siteMetadata: {
    title: `Estamparia Digital em Algodão`,
    description: `Camisetas em algodão com estampas digitais. Só o que precisa. Nada a mais.`,
    author: `Ouro Estamparia Digital`,
    phone: `5548998411163`,
    email: `contato@ouroboros.com.br`,
    models: `12`,
    colors: `30`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-styled-components`,
    {
      resolve: `gatsby-plugin-prefetch-google-fonts`,
      options: {
        fonts: [
          {
            family: `Poppins`,
            variants: [`300`, `400`, `600`, `700`],
          },
        ],
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `geral`,
        path: `${__dirname}/src/images/geral`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `clients`,
        path: `${__dirname}/src/images/clients`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `mix`,
        path: `${__dirname}/src/images/mix`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `estampas`,
        path: `${__dirname}/src/images/estampas`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/ouroestamparia-icon.png`,
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    `gatsby-plugin-offline`,
    // {
    //   resolve: `gatsby-source-googlemaps-static`,
    //   options: {
    //     key: `AIzaSyDJC6ynvrf9Sby0aeVgIARh2mi9u5Tkhj8`,
    //     center: `-27.118729,-48.614171`,
    //     size: `640x320`,
    //     scale: `2`,
    //     mapType: `roadmap`,
    //     language: `portuguese`,
    //     zoom: `15`,
    //     markers: [
    //       {
    //         location: `-27.118729,-48.614171`,
    //         color: `brown`,
    //         label: `O`,
    //       },
    //     ],
    //   },
    // },
  ],
}
