import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

const Image = () => {
  const data = useStaticQuery(graphql`
    query {
      file(sourceInstanceName: { eq: "clients" }, name: { eq: "vermont" }) {
        childImageSharp {
          fluid(maxWidth: 1000) {
            ...GatsbyImageSharpFluid_tracedSVG
          }
        }
      }
    }
  `)

  console.log(data)

  return <Img fluid={data.file.childImageSharp.fluid} />
}

export default Image
