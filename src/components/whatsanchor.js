import React from "react"
import { useStaticQuery, graphql } from "gatsby"

import styled from "styled-components"

const WhatsAnchor = ({ className, children }) => {
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            phone
          }
        }
      }
    `
  )

  const phone = `https://api.whatsapp.com/send?phone=+${site.siteMetadata.phone}&text=Olá, tudo bem?`

  return (
    <StyledAnchor
      className={className}
      href={phone}
      target="_blank"
      rel="noopener norefferer"
    >
      {children}
    </StyledAnchor>
  )
}

export default WhatsAnchor

const StyledAnchor = styled.a`
  font-weight: 500;
  font-size: 14px;
  color: ${props => props.theme.color.white.regular};
  letter-spacing: 1px;
  height: 60px;
  display: block;

  text-transform: uppercase;
  cursor: pointer;
  white-space: nowrap;
  background: ${props => props.theme.color.accent};
  border-radius: 4px;
  padding: 0px 40px;
  border-width: 0px;
  border-style: initial;
  border-color: initial;
  border-image: initial;
  outline: 0px;
  &:hover {
    box-shadow: rgba(110, 120, 152, 0.55) 0px 2px 10px 0px;
  }
  @media (max-width: ${props => props.theme.screen.md}) {
  }
  @media (max-width: ${props => props.theme.screen.sm}) {
    margin-left: 0;
  }

  text-decoration: none;
  display: flex;
  justify-content: center;
  align-items: center;
`
