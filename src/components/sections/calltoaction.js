import React from "react"
import styled from "styled-components"

import { Container, Section } from "../global"
import WhatsAnchor from "../whatsanchor"

const CallToAction = ({ className, children, msg1, msg2, sub }) => {
  return (
    <StyledSection>
      <GetStartedContainer>
        <GetStartedTitle>
          {msg1}
          {msg2 != null ? <br /> : null}
          {msg2}
        </GetStartedTitle>
        <WhatsAnchor>{children}</WhatsAnchor>
        <Subtitle>({sub})</Subtitle>
      </GetStartedContainer>
    </StyledSection>
  )
}

export default CallToAction

const StyledSection = styled(Section)`
  background-color: ${props => props.theme.color.background.light};
  clip-path: polygon(0 0, 100% 5vw, 100% 100%, 0 calc(100% - 5vw));
`

const GetStartedContainer = styled(Container)`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 80px 0 40px;
`

const GetStartedTitle = styled.h3`
  margin: 0 auto 32px;
  text-align: center;
`

const Subtitle = styled.span`
  ${props => props.theme.font_size.xxxsmall}
  padding-top: 8px;
  color: ${props => props.theme.color.primary};
`
