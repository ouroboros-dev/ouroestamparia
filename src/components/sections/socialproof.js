import React from "react"
import styled from "styled-components"

import { Section, Container } from "../global"

import Ouroboros from "../images/ouroboros"
import Ram from "../images/ram"
import Vermont from "../images/vermont"
import Saldanha from "../images/saldanha"

const SocialProof = () => (
  <Section id="socialproof">
    <StyledContainer>
      <Subtitle>Clientes</Subtitle>
      <SocialProofGrid>
        <SocialProofItem>
          <SocialProofTitle>Ouroboros</SocialProofTitle>
          <SocialProofLogo>
            <Ouroboros />
          </SocialProofLogo>
        </SocialProofItem>
        <SocialProofItem>
          <SocialProofTitle>Vermont</SocialProofTitle>
          <SocialProofLogo>
            <Vermont />
          </SocialProofLogo>
        </SocialProofItem>
        <SocialProofItem>
          <SocialProofTitle>RAM Yoga</SocialProofTitle>
          <SocialProofLogo>
            <Ram />
          </SocialProofLogo>
        </SocialProofItem>
        <SocialProofItem>
          <SocialProofTitle>Saldanha JJ</SocialProofTitle>
          <SocialProofLogo>
            <Saldanha />
          </SocialProofLogo>
        </SocialProofItem>
      </SocialProofGrid>
    </StyledContainer>
  </Section>
)

export default SocialProof

const StyledContainer = styled(Container)``

const SocialProofGrid = styled.div`
  width: 80%;

  display: grid;
  grid-template-columns: repeat(4, 1fr);
  margin: 0px auto;
  grid-column-gap: 40px;
  @media (max-width: ${props => props.theme.screen.sm}) {
    grid-template-columns: 1fr;
    grid-row-gap: 40px;
    padding: 0 64px;
  }
`

const SocialProofItem = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`

const SocialProofTitle = styled.h4`
  color: ${props => props.theme.color.white.darker};
  letter-spacing: 0px;
  line-height: 30px;
  margin-bottom: 10px;
`

const SocialProofLogo = styled.div`
  // height: 15vh;
  // min-height: 100px;

  position: relative;

  width: 35%;
  min-width: 75px;
  height: auto;

  margin-top: 16px;

  opacity: 0.55;
`

const Subtitle = styled.h5`
  font-size: 16px;
  color: ${props => props.theme.color.accent};
  letter-spacing: 0px;
  margin-bottom: 12px;
  text-align: center;

  @media (max-width: ${props => props.theme.screen.sm}) {
    margin-bottom: 32px;
  }
`
