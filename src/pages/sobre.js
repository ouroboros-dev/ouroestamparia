import React from "react"

import Layout from "../components/common/layout/layout"
import SEO from "../components/common/layout/seo"
import Navigation from "../components/common/navigation/navigation"

import CallToAction from "../components/sections/calltoaction"
import Body from "../components/sections/sobre"
import Body2 from "../components/sections/sobreMim"
import Footer from "../components/sections/footer"

const CamisetasPage = () => {
  return (
    <Layout>
      <SEO title=" Sobre |" />
      <Navigation />
      <Body></Body>
      <CallToAction
        msg1="Entre em contato e faça um orçamento"
        sub="falar conosco pelo WhatsApp"
      >
        Encomendar
      </CallToAction>
      <Body2></Body2>
      <Footer />
    </Layout>
  )
}

export default CamisetasPage
