import React from "react"

import Layout from "../components/common/layout/layout"
import SEO from "../components/common/layout/seo"
import Navigation from "../components/common/navigation/navigation"

import Header from "../components/sections/homeHeader"
import SocialProof from "../components/sections/socialproof"
import CallToAction from "../components/sections/calltoaction"
import Features from "../components/sections/features"
import Footer from "../components/sections/footer"

const IndexPage = () => (
  <Layout>
    <SEO title=" " />
    <Navigation />
    <Header
      header1="Camisetas de algodão com estampa digital."
      header2="Só o que você precisar."
      subheader="Nosso processo digital libera a restrição de quantidade nos
              pedidos. Quantas estampas, cores e tamanhos precisar."
    />
    <SocialProof />
    <CallToAction
      msg1="Defina cor, quantidade, estampa"
      msg2="e faça o seu pedido"
      sub="os pedidos são por WhatsApp"
    >
      Encomendar
    </CallToAction>
    <Features />
    <CallToAction
      msg1="Entre em contato e faça um orçamento"
      sub="falar conosco pelo WhatsApp"
    >
      Encomendar
    </CallToAction>
    <Footer />
  </Layout>
)

export default IndexPage
