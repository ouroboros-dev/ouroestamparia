import React from "react"

import Layout from "../components/common/layout/layout"
import SEO from "../components/common/layout/seo"
import Navigation from "../components/common/navigation/navigation"
import Body from "../components/sections/contato"
import Footer from "../components/sections/footer"

const CamisetasPage = () => {
	return (
		<Layout>
			<SEO title=" Contato |" />
			<Navigation></Navigation>
			<Body></Body>
			<Footer />
		</Layout>
	)
}

export default CamisetasPage
